var express    = require("express"),
    app        = express(),
    bodyParser = require("body-parser"),
    mongoose   = require("mongoose"),
    Blog       = require("./model/blog"),
    Category   = require("./model/category"),
    User       = require("./model/admin"),
    flash      = require("connect-flash"),
    session    = require("express-session"),
    cookieParser = require("cookie-parser"),
    methodOver = require("method-override"),
    passport   = require("passport"),
    localStrategy = require("passport-local"),
    locus       = require("locus"),
    slug       = require("slug"); 

        // ===================
        // Mongoose Connection
        // ===================
    mongoose.connect('mongodb://localhost/BLOG',{'useNewUrlParser': true,'useUnifiedTopology':true});
    app.use(express.static(__dirname + '/public'));
    app.use(bodyParser.urlencoded({extended: true}));
    app.set("view engine","ejs");
    app.use(flash()); 
    app.use(methodOver("_method"));
    app.use(cookieParser());
    app.use(require("express-session")({
        secret:"Hello this is Secret",
        resave:false,
        saveUninitialized:false
    }));

    app.use(passport.initialize());
    app.use(passport.session());
    passport.use(new localStrategy(User.authenticate()));
    passport.serializeUser(User.serializeUser());
    passport.deserializeUser(User.deserializeUser());

    app.use(function(req,res,next){
     res.locals.currentUser = req.user;
     res.locals.success     = req.flash("success");
     res.locals.error       = req.flash("error");
     next();
    });

        // ================
        // Main Page
        // ================
    app.get("/",function(req,res){
      Blog.find({},function(err,allblog){
             if(err){
                console.log(err);
             }else{
                console.log(allblog);
             res.render("main",{blog:allblog});
            }
         });
    });

        // ================
        // Error Page
        // ================
    app.get("/errorpage",function(req,res){
        res.render("errorpage");
    });


    app.get("/search",function(req,res){
  
        if(req.query.search){
         Blog.find({title:req.query.search},function(err,allblog){
            if(err){
             console.log(err)
            // res.redirect("errorpage");
             }else{
               if(allblog.length<1){
                res.redirect("errorpage");
                }else{
                res.render("homepage",{blogs:allblog});
                }
            }
        })
    }}); 

        // ================
        // Add New Blog
        // ================

    app.get("/addblog/new",isLoggedIn,function(req,res){
        Category.find({}, function(err,allcategory){
             if(err){
             console.log(err);
            }else{
             console.log(allcategory);
            res.render("new",{categories:allcategory});
         }
        });
    });

    app.post("/addblog",isLoggedIn,function(req,res){
        var title = req.body.title; 
        var image = req.body.image;
        var detail= req.body.detail;
        var category = req.body.Category;
        var date     = req.body.date;
        var titleSlug = slug(title);
        console.log(slug(title));
        var addBlog = {title:title,image:image,detail:detail,category:category,date:date,titleSlug:titleSlug};

   Blog.create(addBlog,function(err, newlyCreated){
        if(err){
         console.log(err);
        }else{
         console.log(newlyCreated);
            res.redirect("/");
         }
        });
    });

        // ================
        // Admin Panel
        // ================

    app.get("/adminpanel",isLoggedIn,function(req,res){
    res.render("adminpanel");
    });

    app.get("/show/:id",function(req,res){
     Blog.findOne({titleSlug: req.params.id}, function(err,foundblog){
            if(err){
            console.log(err);
             res.render("errorpage");
             }else{
              if(! foundblog){
                return   res.render("errorpage");
             }
              console.log(foundblog);
            res.render("show",{blogs:foundblog});
         }
        });
    });

        // ================
        // Add Category
        // ================

    app.get("/addcategory",isLoggedIn,function(req,res){
     res.render("addcategory");
    });

    app.post("/addcategory",isLoggedIn,function(req,res){
     Category.create(req.body,function(err, newlyCreated){
         if(err){
            console.log(err);
            }else{
            console.log(newlyCreated);
            res.redirect("/");
            }
        }); 
    });

        // ================
        // All Category
        // ================

    app.get("/allcategory",isLoggedIn,function(req,res){
      Category.find({}, function(err,allcategory){
            if(err){
            console.log(err);
            }else{
            console.log(allcategory);
            res.render("allcategory",{categories:allcategory});
            }
         });  
    });

    app.get("/editcategory/:id",isLoggedIn,function(req,res){
     Category.findById(req.params.id,function(err,allcategory){
         if(err){
             console.log(err);
         }else{
            console.log(allcategory);
            res.render("editcategory",{categories:allcategory});
         }
        });
    });

        // ================
        // Update Category
        // ================


    app.put("/editcategory/:id",isLoggedIn,function(req,res){
     Category.findByIdAndUpdate(req.params.id,req.body,function(err){
            if(err)
             {
              console.log(err);
             }else{
              console.log(req.body);
                res.redirect("/allcategory");
             }
        });
    });

        // ================
        // show all Blog
        // ================

    app.get("/allblog",isLoggedIn,function(req,res){
     Blog.find({}, function(err,blog){
         if(err){
            console.log(err);
         }else{
            console.log(blog);
            res.render("allblog",{blogs:blog});
         }
        });  
    });

    app.get("/editblog/:id",isLoggedIn,function(req,res){
     Blog.findById(req.params.id, function(err,blog){
          if(err){
            console.log(err);
          }else{
                Category.find({},function(err,categories)
                {
                     if(err){
                     console.log(err);
                    }else{
                    console.log("edit",blog);
                    res.render("editallblog",{Blog:blog,categories:categories});
                 }
                 });
             }
        });  
    });

        // ================
        // Update Blog
        // ================

    app.put("/editblog/:id",isLoggedIn,function(req,res){
     Blog.findByIdAndUpdate(req.params.id,req.body,function(err){
            if(err)
             {
             console.log(err);
             }else{
              res.redirect("/");
             }
        });
    });

        // ================
        // Delete Blog
        // ================

    app.get("/delete/:id",isLoggedIn, function(req,res){
      Blog.deleteOne({_id:req.params.id},function(err){
            if(err){
             console.log(err);
             }else{
              res.redirect("/");
            }
         });
    });

        // ================
        // Delete Category
        // ================

    app.get("/delete/category/:id",isLoggedIn, function(req,res){
     Category.deleteOne({_id:req.params.id},function(err){
            if(err){
              console.log(err);
            }else{
                res.redirect("/allcategory");
            }
        });
    });

        // ================
        // Register User
        // ================

    app.get("/register",function(req,res){
     res.render("register");
    });

    app.post("/register",function(req,res){
        var username = req.body.username;
        var password = req.body.password;
         User.register(new User({username:req.body.username}),req.body.password,function(err,user){
             if(err){
              console.log(err);
             }else{
              console.log("user created successfully");
              res.redirect("/login")
             }
         });
    });

        //=================
        //Login User
        //=================

    app.get("/login",function(req,res){
        res.render("login", {message: req.flash("error")});
    });

    app.post("/login",passport.authenticate("local",
    {
         successRedirect: "/adminpanel",
         failureRedirect:"/login"
    }),function(req,res){
    });

        //=================
        //Logout
        //=================

    app.get("/logout",function(req,res){
         req.logout();
         req.flash("success","Logged you out Successfully!");
         res.redirect("/");
    });

        //==============
        //middleware
        //==============

    function isLoggedIn(req,res,next){
         if(req.isAuthenticated()){
          return next();
         }
          req.flash("error","Please Login First ")
          res.redirect("/login");
    }

        //=====================
        //Server Listening Port
        //=====================

    let port = 8080;
    app.listen(port,function(){
    console.log("Server is started Listening at Port",port);
    });
