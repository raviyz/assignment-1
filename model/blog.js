var mongoose = require('mongoose');


var blogSchema = new mongoose.Schema({
    title:String,
    image:String,
    detail:String,
    category:String,
    titleSlug:String,
    created:{type:Date,default:Date.now}
   
    });


module.exports = mongoose.model("Blog",blogSchema);
